import Vue from 'vue'
import VueRouter from 'vue-router'
import {routes} from './routes'
import App from './App.vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import BootstrapVue from 'bootstrap-vue'
import VueSession from 'vue-session'
import VueHead from 'vue-head'
import VeeValidate from 'vee-validate'
Vue.use(VueSession)
Vue.use(BootstrapVue)
Vue.use(VueAwesomeSwiper)
Vue.use(VueHead)
Vue.use(VueRouter)
Vue.use(VeeValidate)

//ROUTES
const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior(to, from, savedPsoition) {
    const position = {}
    position.x = 0
    position.y = 0
    return position
  }
})

const baseAPI = 'ws.conectagro.com'

// CART 
const config = {
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'fields', 
  delay: 0, 
  locale: 'en', 
  dictionary: null, 
  strict: true, 
  classes: false, 
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true
};

const eventBus = new Vue()

Vue.component('listado-productos', {
  props: ['productos'],
  template: `
        <section>
            <ul>
                <li v-for="producto in productos">
                    {{ producto.nombre }} -
                    <small>{{ producto.precio.toFixed(2) }} €</small>
                    <button @click="eliminarProducto(producto.precio)">-</button>
                    <button @click="anadirProducto(producto.precio)">+</button>
                </li>
            </ul>
        </section>`,
  methods: {
    anadirProducto (precio) {
      eventBus.$emit('anadir', precio)
    },
    eliminarProducto (precio) {
      eventBus.$emit('eliminar', precio)
    }
  }
})

Vue.component('listado-categorias', {
  props: ['categorias'],
  template: `
    <div class="card">
        <div class="card-body">
            <ul class="list-unstyled">
                <li v-for="categoria in categorias">
                    <router-link :to="'/categoria/'+ categoria.id "> {{ categoria.nombre }}</router-link>
                </li>
            </ul>
        </div>
    </div>
    `
})

Vue.component('productos-destacados', {
  props: ['productos'],
  template: `
    <div class="row">
        <article class="col-md-3 col-sm-4 col-6 mg-top-15 mg-bottom-15" v-for="producto in productos">
            <div class="card">
                <div class="card-picture align-items-center">
                  <router-link :to="'/producto/'+ producto.id"><img class="card-img-top" :src="producto.imagen_ruta" :alt="producto.nombre" v-show="producto.imagen_ruta != null"></router-link>
                </div>
                <div class="card-body">
                    <h2>
                        <router-link :to="'/producto/'+ producto.id" v-html="producto.nombre"></router-link>
                    </h2>
                    <p>S/. {{ producto.precio }}</p>
                    <b-button variant="primary" @click="anadirProducto(producto.precio)">
                        <img :src="iconCart" width="18" alt="">
                    </b-button>
                </div>
            </div>
        </article>
    </div>`,
  data: () => ({
    iconCart: require('./assets/svg/cart.svg')
  }),
  methods: {
    anadirProducto (precio) {
      eventBus.$emit('anadir', precio)
    },
    eliminarProducto (precio) {
      eventBus.$emit('eliminar', precio)
    }
  }
})

Vue.component('carrito-compra', {
  template: `
        <b-nav-item to="/contacto" right>
            <span class="badge">{{ cantidadProductos }}</span> <img :src="iconCart" width="24">
        </b-nav-item>`,
  data: () => ({
    cantidadProductos: 0,
    total: 0,
    iconCart: require('./assets/svg/cart.svg')
  }),
  created () {
    eventBus.$on('anadir', (precio) => {
      if (this.total >= 0) {
        this.total += precio
        this.cantidadProductos++
      }
    })
    eventBus.$on('eliminar', (precio) => {
      if (this.total > 0) {
        this.total -= precio
        this.cantidadProductos--
      }
    })
  }
})

new Vue({
  el: '#app',
  // data: () => ({
  //   imgnone: require('../assets/img/img-none.jpg')
  // }),
  router,
  render: h => h(App)
})

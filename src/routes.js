const Home = resolve => {
    require.ensure(['./components/Home.vue'], () => {
        resolve(require('./components/Home.vue'))
    })
}

const Contacto = resolve => {
    require.ensure(['./components/Contacto.vue'], () => {
        resolve(require('./components/Contacto.vue'))
    })
}

const Registro = resolve => {
    require.ensure(['./components/Registro.vue'], () => {
        resolve(require('./components/Registro.vue'))
    })
}

const Login = resolve => {
    require.ensure(['./components/Login.vue'], () => {
        resolve(require('./components/Login.vue'))
    })
}

const PageNotFound = resolve => {
    require.ensure(['./components/statics/PageNotFound.vue'], () => {
        resolve(require('./components/statics/PageNotFound.vue'))
    })
}

const Put = resolve => {
    require.ensure(['./components/Put.vue'], () => {
        resolve(require('./components/Put.vue'))
    })
}

export const routes = [
    {path: '/', component: Home},
    {path: '/contacto', component: Contacto},
    {path: '/registro', component: Registro},
    {path: '/login', component: Login},
    {path: '/put', component: Put},
    {path: '*', component: PageNotFound }
]

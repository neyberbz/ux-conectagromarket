webpackJsonp([4],{

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(125)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(92),
  /* template */
  __webpack_require__(119),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)();
// imports


// module
exports.push([module.i, ".mg-top-15{margin-top:15px}.mg-bottom-15{margin-bottom:15px}.pd_25{padding-top:25px;padding-bottom:25px}.pd_50{padding-top:50px;padding-bottom:50px}.bg_blank{background:#fff}.badge{border-radius:50px;background:#83b94c}.formRegistro{padding:50px 0}.formRegistro .form-group label{text-align:left}.formRegistro .form-group label span{color:#555;font-size:14px;font-weight:700}.formRegistro .form-group input{border:0;font-size:12px;border-radius:0;padding-left:0;padding-right:0;border-bottom:2px solid #ef6e05}.formRegistro button{border:0;color:#fff;margin:20px 0;font-size:12px;font-weight:700;letter-spacing:1px;background:#83b94c}.formRegistro button:hover{background:#83b94c}.formRegistro p{font-size:12px}.formRegistro .redes a{font-size:12px;font-weight:700}", ""]);

// exports


/***/ }),

/***/ 119:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "registro",
    attrs: {
      "id": "registro"
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "row justify-content-md-center"
  }, [_c('div', {
    staticClass: "col-md-6 col-12 content_main"
  }, [_c('form', {
    staticClass: "formRegistro",
    on: {
      "submit": _vm.onSubmit
    }
  }, [_c('b-form-group', {
    staticClass: "col-6 float-left",
    attrs: {
      "id": "InputGroup4",
      "label": "Correo electrónico:",
      "label-for": "inputEmail"
    }
  }, [_c('b-form-input', {
    attrs: {
      "id": "inputEmail",
      "type": "email",
      "required": "",
      "placeholder": "Ingresar correo electrónico..."
    },
    model: {
      value: (_vm.form.email),
      callback: function($$v) {
        _vm.form.email = $$v
      },
      expression: "form.email"
    }
  })], 1), _vm._v(" "), _c('b-form-group', {
    staticClass: "col-6 float-right",
    attrs: {
      "id": "InputGroup5",
      "label": "Contraseña:",
      "label-for": "inputPwd"
    }
  }, [_c('b-form-input', {
    attrs: {
      "id": "inputPwd",
      "type": "password",
      "required": "",
      "placeholder": "Ingresar contraseña..."
    },
    model: {
      value: (_vm.form.pwd),
      callback: function($$v) {
        _vm.form.pwd = $$v
      },
      expression: "form.pwd"
    }
  })], 1), _vm._v(" "), _c('b-button', {
    staticClass: "text-uppercase",
    attrs: {
      "type": "submit",
      "variant": "primary"
    }
  }, [_vm._v("Ingresar")]), _vm._v(" "), _c('p', [_c('router-link', {
    attrs: {
      "to": "/"
    }
  }, [_vm._v("¿olvide mi contraseña?")])], 1), _vm._v(" "), _c('p', [_vm._v("\n                        ¿Nuevo aquí? "), _c('router-link', {
    attrs: {
      "to": "/registro"
    }
  }, [_vm._v("REGISTRATE")])], 1)], 1)])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "carousel fade-carousel slide",
    attrs: {
      "id": "carouselPrincipal",
      "data-ride": "carousel",
      "data-interval": "3000"
    }
  }, [_c('div', {
    staticClass: "carousel-inner",
    attrs: {
      "role": "listbox"
    }
  }, [_c('div', {
    staticClass: "carousel-caption"
  }, [_c('h4', {
    staticClass: "text-center text-uppercase"
  }, [_vm._v("Iniciar sesión")]), _vm._v(" "), _c('h6', {
    staticClass: "text-center"
  }, [_vm._v("Miles de productos agricolas a un clic")])])])])])])
}]}

/***/ }),

/***/ 125:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(101);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("4c0d5d6a", content, true);

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: () => ({
    form: {
      username: '',
      password: ''
    },
    title: 'Inicio de Sesión'
  }),
  head: {
    // To use "this" in the component, it is necessary to return the object through a function
    title: function () {
      return {
        inner: this.title
      };
    },
    meta: [{ name: 'description', content: 'My description', id: 'desc' }]
  },
  methods: {
    onSubmit(evt) {
      evt.preventDefault();
      alert(JSON.stringify(this.form));
    }
  }
});

/***/ })

});
//# sourceMappingURL=4.build.js.map
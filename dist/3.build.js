webpackJsonp([3],{

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)();
// imports


// module
exports.push([module.i, ".mg-top-15{margin-top:15px}.mg-bottom-15{margin-bottom:15px}.pd_25{padding-top:25px;padding-bottom:25px}.pd_50{padding-top:50px;padding-bottom:50px}.bg_blank{background:#fff}.badge{border-radius:50px;background:#83b94c}.formRegistro{padding:50px 0}.formRegistro .form-group label{text-align:left}.formRegistro .form-group label span{color:#555;font-size:14px;font-weight:700}.formRegistro .form-group input{border:0;font-size:12px;border-radius:0;padding-left:0;padding-right:0;border-bottom:2px solid #ef6e05}.formRegistro button{border:0;color:#fff;margin:20px 0;font-size:12px;font-weight:700;letter-spacing:1px;background:#83b94c}.formRegistro button:hover{background:#83b94c}.formRegistro p{font-size:12px}.formRegistro .redes a{font-size:12px;font-weight:700}", ""]);

// exports


/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(126)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(94),
  /* template */
  __webpack_require__(120),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),

/***/ 120:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "registro",
    attrs: {
      "id": "registro"
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "row justify-content-md-center"
  }, [_c('div', {
    staticClass: "col-md-6 col-12 content_main"
  }, [_c('form', {
    staticClass: "formRegistro",
    on: {
      "submit": _vm.onSubmit
    }
  }, [_c('div', {
    staticClass: "form-group col-6 float-left",
    attrs: {
      "id": "InputGroup1"
    }
  }, [_c('label', {
    attrs: {
      "for": "inputUsuario"
    }
  }, [_vm._v("Correo Electrónico")]), _vm._v(" "), _c('p', {
    class: {
      'control': true
    }
  }, [_c('input', {
    directives: [{
      name: "validate",
      rawName: "v-validate",
      value: ('required|email'),
      expression: "'required|email'"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.form.email),
      expression: "form.email"
    }],
    class: {
      'input form-control': true, 'is-danger': _vm.errors.has('email')
    },
    attrs: {
      "name": "email",
      "id": "inputUsuario",
      "type": "email",
      "placeholder": "Ingresar correo electrónico..."
    },
    domProps: {
      "value": (_vm.form.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.form.email = $event.target.value
      }
    }
  }), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.errors.has('email')),
      expression: "errors.has('email')"
    }],
    staticClass: "help is-danger"
  }, [_vm._v(_vm._s(_vm.errors.first('email')))])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-6 float-left",
    attrs: {
      "id": "InputGroup2"
    }
  }, [_c('label', {
    attrs: {
      "for": "inputPwd"
    }
  }, [_vm._v("Contraseña")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.password),
      expression: "form.password"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "inputPwd",
      "type": "password",
      "required": "",
      "placeholder": "Ingresar contraseña..."
    },
    domProps: {
      "value": (_vm.form.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.form.password = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('p', [_vm._v("Al registrarme acepto los termino y condiciones de Conectagro Market")])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "carousel fade-carousel slide",
    attrs: {
      "id": "carouselPrincipal",
      "data-ride": "carousel",
      "data-interval": "3000"
    }
  }, [_c('div', {
    staticClass: "carousel-inner",
    attrs: {
      "role": "listbox"
    }
  }, [_c('div', {
    staticClass: "carousel-caption"
  }, [_c('h4', {
    staticClass: "text-center text-uppercase"
  }, [_vm._v("Registro")]), _vm._v(" "), _c('h6', {
    staticClass: "text-center"
  }, [_vm._v("Miles de productos agricolas a un clic")])])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group col-12 float-left text-center"
  }, [_c('button', {
    staticClass: "btn text-uppercase",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Registrarme")])])
}]}

/***/ }),

/***/ 126:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(102);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("e206f23e", content, true);

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: () => ({
    form: {
      email: '',
      password: ''
    },
    title: 'Registro'
  }),
  head: {
    // To use "this" in the component, it is necessary to return the object through a function
    title: function () {
      return {
        inner: this.title
      };
    },
    meta: [{ name: 'description', content: 'My description', id: 'desc' }]
  },
  methods: {
    onSubmit(evt) {
      evt.preventDefault();
      alert(JSON.stringify(this.form));
    }
  }
});

/***/ })

});
//# sourceMappingURL=3.build.js.map